﻿1
00:00:01,700 --> 00:00:03,090
Hola,

2
00:00:03,100 --> 00:00:06,690
soy RaulMacho y es voy realizar la presentación de mi trabajo final de grado

3
00:00:06,700 --> 00:00:10,790
como parte del plan docente del grado de Ingeniería Informática el título del

4
00:00:10,800 --> 00:00:14,190
proyecto es integración despliegue y mantenimiento de micros sitios basados

5
00:00:14,200 --> 00:00:18,690
en contenedores en lareas aplicaciones y sistemas distribuidos mi tutor

6
00:00:18,700 --> 00:00:21,990
es Amadeo Albos y responsable de la asignatura es Joan Manuel marqués

7
00:00:22,000 --> 00:00:26,290
el recorrido que vamos a seguir es el siguiente que corresponde a las fases

8
00:00:26,300 --> 00:00:29,690
de desarrollo que se han seguido tanto en el proyecto como en la memoria de esta

9
00:00:29,700 --> 00:00:34,090
forma se tratan todos los aspectos fundamentales del proyecto comenzaremos con

10
00:00:34,100 --> 00:00:35,290
una introducción a nuestro proyecto.

11
00:00:35,300 --> 00:00:39,890
Lo primo primero que debemos hacer es cuál es el contexto y la justificación del

12
00:00:39,900 --> 00:00:44,490
proyecto son varias las razones que justifican el desarrollo de este proyecto por

13
00:00:44,500 --> 00:00:48,090
un lado tenemos el auje de los microservicios cada vez más empleados en diferentes tipos

14
00:00:48,100 --> 00:00:50,990
de aplicaciones tradicionalmente monolíticas por

15
00:00:51,000 --> 00:00:53,090
otro lado la mejora del rendimiento de los contenedores,

16
00:00:53,100 --> 00:00:56,490
además son cada vez más los fabricantes que ofrecen sus herramientas

17
00:00:56,500 --> 00:01:00,390
y  doquerizadas para este tipo de entornos esto nos lleva al uso

18
00:01:00,400 --> 00:01:01,590
de arquitecturas el proyecto.

19
00:01:01,600 --> 00:01:06,190
De cada vez mayor envergadura adicionalmente el surgimiento de los orquestadores

20
00:01:06,200 --> 00:01:11,490
ha permitido un uso aún mayor en grandes proyectos con múltiples servidores o muy

21
00:01:11,500 --> 00:01:17,290
complejos en base a lo anteriormente expuesto podemos definir el líneas generales que

22
00:01:17,300 --> 00:01:21,590
el objetivo principal es crear una plataforma que permita el despliegue

23
00:01:21,600 --> 00:01:25,490
de aplicaciones sobre contenedores en un sistema distribuido de varios nudos

24
00:01:25,500 --> 00:01:31,590
un aspecto que cada día es de mayor importancia es el impacto que todo proyecto tiene

25
00:01:31,600 --> 00:01:36,590
la sociedad y la sostenibilidad a continuación se muestran los puntos más importantes

26
00:01:36,600 --> 00:01:38,290
que involucra a nuestro proyecto,

27
00:01:38,300 --> 00:01:41,790
además en estos puntos de fricción mostramos las acciones

28
00:01:41,800 --> 00:01:45,790
mitigadoras por un lado en el consumo y producción responsable una

29
00:01:45,800 --> 00:01:49,590
vez finalizada la vida útil del ordenador se debe depositar en un punto limpio.

30
00:01:49,600 --> 00:01:52,890
Además se debe alargar la vida de todos los componentes

31
00:01:52,900 --> 00:01:56,690
siempre que sea posible por otro lado para reducir las

32
00:01:56,700 --> 00:02:01,290
desigualdades se plantea utilizar ordenadores adaptados a personas con difícil.

33
00:02:01,600 --> 00:02:06,090
sueles utilizar las herramientas de control de accesibilidad de Microsoft

34
00:02:06,100 --> 00:02:10,990
Word para la creación de la memoria y incluir los subtítulos en formato SRT

35
00:02:11,000 --> 00:02:14,390
tanto de este vídeo como el vídeo explicativo de funcionamiento de

36
00:02:14,400 --> 00:02:19,290
la plataforma esto será subidos al repositorio git con el resto de los anexos del proyecto

37
00:02:20,800 --> 00:02:25,090
Cuál es la metodología utilizada es la metodología en cascada

38
00:02:25,100 --> 00:02:29,290
Se descarto  la metodología ágil por limitaciones

39
00:02:29,300 --> 00:02:32,190
personales y discrepancias con los horarios con el equipo docente

40
00:02:32,200 --> 00:02:35,690
y el alumno lo que impedía llevar a cabo las reuniones diarias propia

41
00:02:35,700 --> 00:02:39,490
de esta metodología no decantamos por lo tanto por una metodología en cascada,

42
00:02:39,500 --> 00:02:43,190
que se adapta mejor a las características del proyecto minimizando los

43
00:02:43,200 --> 00:02:47,990
riesgos en su desarrollo y mejora la planificación el proyecto se desarrolla,

44
00:02:48,000 --> 00:02:51,490
por lo tanto siguiendo las etapas propias de esta metodología durante

45
00:02:51,500 --> 00:02:54,590
el desarrollo del proyecto se han establecido cuatro, entregas o hitos.

46
00:02:56,100 --> 00:02:59,890
Qué que abarcan diferentes fases del desarrollo del proyecto?

47
00:02:59,900 --> 00:03:01,390
Cómo podemos ver a continuación

48
00:03:01,400 --> 00:03:07,790
Veremos más en detalle la planificación de estas tareas decir que una

49
00:03:07,800 --> 00:03:12,990
planificación concreta y realista es clave para el desarrollo correcto y en plazo de

50
00:03:13,000 --> 00:03:15,090
un proyecto de cómo esté.

51
00:03:16,000 --> 00:03:19,690
aquí podemos ver la planificación de las tareas previstas para el desarrollo

52
00:03:19,700 --> 00:03:24,390
completo del proyecto diferenciadas por cada hito o entregarle la estimación de horas,

53
00:03:24,400 --> 00:03:29,490
es de 300 donde la mayor parte de carga de trabajo se estima la entrega 2 y 3,

54
00:03:29,500 --> 00:03:31,990
dónde se realiza la implementación y validación del proyecto,

55
00:03:32,000 --> 00:03:36,490
el análisis y cumplimiento de dicha planificación será analizado al final de esta

56
00:03:36,500 --> 00:03:41,890
presentación el reparto de las tareas representado en Diagrama de GANT es el siguiente

57
00:03:41,900 --> 00:03:45,990
en el entregable uno se hizo un estudio de la viabilidad y definición de los requisitos

58
00:03:46,000 --> 00:03:51,590
en el entregable 2 análisis y diseño de la plataforma entregable 3 implementación

59
00:03:51,600 --> 00:03:55,790
y validación de la plataforma y por último en el cuarto entregable la resolución de las

60
00:03:55,800 --> 00:04:01,890
anomalías relación final de la memoria y elaboración de esta presentación pasamos a

61
00:04:01,900 --> 00:04:06,890
la fase de análisis durante esta etapa se realizaron las necesidades la viabilidad y los

62
00:04:06,900 --> 00:04:12,090
riesgos fundamentales del proyecto comenzamos con análisis de funcionamiento esperado

63
00:04:12,100 --> 00:04:13,590
de nuestro producto es decir,

64
00:04:13,600 --> 00:04:18,390
cuál es el funcionamiento que queremos que Final tenga en reglas generales los

65
00:04:18,399 --> 00:04:22,490
aspectos funcionales que deben cubrir son los siguientes emular un entorno

66
00:04:22,500 --> 00:04:26,290
de producción distribuido crear un entorno que permita el despliegue y uso

67
00:04:26,300 --> 00:04:30,090
de aplicaciones basadas en contenedores y administrados mediante un orquestador

68
00:04:30,100 --> 00:04:34,390
y integrar y valorar las diferentes herramientas que ofrece el mercado y analizar

69
00:04:34,400 --> 00:04:40,590
es cuál se adecua mejor a nuestras necesidades basándonos en este análisis funcional

70
00:04:40,600 --> 00:04:44,490
del proyecto se han descrito una serie de requerimientos que definen y concretizan

71
00:04:44,500 --> 00:04:48,290
las características de este además se ha creado una serie de variaciones que

72
00:04:48,300 --> 00:04:53,590
certifican su cumplimiento en base a estos requerimientos en unas fases elegirán las

73
00:04:53,600 --> 00:04:55,790
herramientas y soluciones a integrar en la plataforma.

74
00:04:57,400 --> 00:05:00,390
Igualmente para minimizar los riesgos de fracaso del proyecto

75
00:05:00,400 --> 00:05:03,690
se realizó un estudio de los riesgos principales e implementaron

76
00:05:03,700 --> 00:05:07,190
acciones mitigadoras para reducir en la medida de lo posible estos riesgos,

77
00:05:07,200 --> 00:05:11,090
aunque se detalle la la mayoría de estos riesgos derivan de

78
00:05:11,100 --> 00:05:14,990
la experiencia en la materia y pueden ser subsanados con dedicacion

79
00:05:15,000 --> 00:05:19,490
extra y consultas con el equipo docente te hacen análisis y

80
00:05:19,500 --> 00:05:22,490
la definición de los requisitos se realizó el diseño del producto

81
00:05:22,500 --> 00:05:23,490
en base a estas exigencias.

82
00:05:24,800 --> 00:05:27,490
Cómo primer paso de esta fase de hemos situados

83
00:05:27,500 --> 00:05:29,890
en el estado actual de las tecnologías que nos implican?

84
00:05:31,100 --> 00:05:34,690
En este estudio se centrará en varias tecnologías por un lado el estudio

85
00:05:34,700 --> 00:05:38,790
de las máquinas virtuales y contenedores ambas se han utilizado en este

86
00:05:38,800 --> 00:05:41,290
en el desarrollo de la arquitectura del proyecto debido a sus características,

87
00:05:41,300 --> 00:05:45,890
además ya herramientas externas que aporten en las capacidades necesarias

88
00:05:45,900 --> 00:05:50,090
para nuestra plataforma más concretamente herramientas para el control

89
00:05:50,100 --> 00:05:53,390
de versiones y despliegue automático herramientas para el análisis

90
00:05:53,400 --> 00:05:57,890
de seguridad de imágenes y con y por último software con interfaz gráfica

91
00:05:57,900 --> 00:06:03,190
para el monitoreo del estado de aplicaciones contenerizadas tras

92
00:06:03,200 --> 00:06:06,190
el estudio de dichas soluciones se diseña la arquitectura de las

93
00:06:06,200 --> 00:06:07,990
plataformas con soluciones elegidas.

94
00:06:09,300 --> 00:06:12,490
está quitectura, está formada por las siguientes herramientas por un lado,

95
00:06:12,500 --> 00:06:16,490
tendremos un ordenador físico con Windows 10 sobre el que se quitará las siguientes

96
00:06:16,500 --> 00:06:20,890
utilidades primeramente VirtualBox utilizado para crear y administrar las máquinas

97
00:06:20,900 --> 00:06:25,090
virtuales Ubuntu que será el sistema operativo de todas las máquinas virtuales

98
00:06:25,100 --> 00:06:29,590
Docker que será el encargado de administrar los contenedores y dentro de Docker

99
00:06:29,600 --> 00:06:34,190
utilizaremos varias herramientas por un lado Trivy y Docker securuty bench, estas

100
00:06:34,200 --> 00:06:37,090
herramientas son utilizadas para el análisis de vulnerabilidades de seguridad

101
00:06:37,100 --> 00:06:41,090
Gitlab que se emplea como herramienta para el versionado y el despliegue automático

102
00:06:41,100 --> 00:06:45,990
y portainer que se la interfaz gráfica utilizada para gestión y diagnóstico de los

103
00:06:46,000 --> 00:06:47,690
contenedores y servicios Docker

104
00:06:48,900 --> 00:06:51,490
Una vez decidido el diseño se produce a

105
00:06:51,500 --> 00:06:53,890
la implementación integración de todos los componentes.

106
00:06:55,700 --> 00:06:59,890
La arquitectura final estará formada por 4 máquinas virtuales y será

107
00:06:59,900 --> 00:07:04,190
representativa de un sistema distribuido sobre máquinas físicas contamos con

108
00:07:04,200 --> 00:07:08,090
3 nodos Docker que formarán el cluster Docker Swarm y una máquina que será

109
00:07:08,100 --> 00:07:12,690
el servidor NFS dónde se almacenarán los datos persistentes y accesibles

110
00:07:12,700 --> 00:07:14,390
y comunes a todos los nodos.

111
00:07:15,900 --> 00:07:19,690
tras la integración de todos los componentes de la plataforma se realizan

112
00:07:19,700 --> 00:07:23,990
los test que verifican que el comportamiento es el esperado en diferentes configuraciones

113
00:07:26,400 --> 00:07:31,090
Esta etapa se desarrollará en tres fases, primero definiremos el flujo del trabajo,

114
00:07:31,100 --> 00:07:35,390
después explicaremos la aplicación elegida para las pruebas para por

115
00:07:35,400 --> 00:07:39,090
último realizar las pruebas en diferentes escenarios y configuraciones.

116
00:07:40,600 --> 00:07:42,190
Como decíamos la plataforma,

117
00:07:42,200 --> 00:07:44,990
debe ser capaz de realizar el proceso completo de despliegue de aplicaciones,

118
00:07:45,000 --> 00:07:49,690
por eso vamos a empezar comenzando con el despliegue de estas aplicaciones.

119
00:07:51,100 --> 00:07:56,190
Por un lado el equipo de desarrollo y será el encargado de entregar las

120
00:07:56,200 --> 00:08:00,090
imágenes Docker de las aplicaciones y deberá llevar a cabo las pruebas

121
00:08:00,100 --> 00:08:04,890
necesarias antes de ello una vez el equipo de operaciones y despliegue recibe

122
00:08:04,900 --> 00:08:09,190
estas imágenes podra comenzar a el flujo propio que afecta este proyecto

123
00:08:09,200 --> 00:08:13,090
en este caso se recepcionará las imágenes se analizarán las vulnerabilidades

124
00:08:13,100 --> 00:08:14,990
de estas imágenes sí,

125
00:08:15,000 --> 00:08:18,490
encuentra una vulnerabilidad es se devolverá al equipo

126
00:08:18,500 --> 00:08:20,290
de desarrollo para que la solucione, y  si todo es correcto,

127
00:08:20,300 --> 00:08:25,090
se almacenan las las imágenes en el repositorio de la plataforma y etiquetada

128
00:08:25,100 --> 00:08:30,090
en correctamente posteriormente actualizará el código de repositorios principalmente

129
00:08:30,100 --> 00:08:35,690
el docker-compose de configuración y se configurará el despliegue del CI/CD

130
00:08:35,700 --> 00:08:37,390
y pipeline de gitlab.

131
00:08:37,900 --> 00:08:42,190
Una vez se despliega la aplicación mediante la aplicación

132
00:08:42,200 --> 00:08:47,290
de portainer podemos comprobar el funcionamiento de nuestra aplicación desplegada.

133
00:08:48,700 --> 00:08:51,790
A continuación vamos a explicar la aplicación

134
00:08:51,800 --> 00:08:54,290
empleada para las pruebas y validaciones de la plataforma,

135
00:08:54,300 --> 00:08:58,090
la aplicación elegida para verificar el comportamiento de

136
00:08:58,100 --> 00:09:01,990
la plataforma en la aplicación Vote app de esta aplicación

137
00:09:02,000 --> 00:09:05,590
nos permite votar por nuestra mascota favorita y consta de dos interfaces.

138
00:09:06,600 --> 00:09:10,590
Usuario  y resultados nos permite al usuario votar por

139
00:09:10,600 --> 00:09:14,490
su mascota favorita y está desarrollada en python nos permite

140
00:09:14,500 --> 00:09:19,890
al usuario verificar los resultados de las votaciones y está desarrollada en NodeJs,

141
00:09:19,900 --> 00:09:24,090
la aplicación vete incluye tres aplicaciones internas que realizan otras tareas,

142
00:09:24,100 --> 00:09:27,990
la primera ellas es Redis que se encarga de recoger los votos recibidos

143
00:09:28,000 --> 00:09:32,590
y crea una cola la segunda es worker que recoge los votos de la coda Redis

144
00:09:32,600 --> 00:09:37,490
y los almacena en la base de datos postgres por último tenemos la base de datos

145
00:09:37,500 --> 00:09:42,290
postgres y además añadido la funcionalidad adminer una herramienta para ver el contenido.

146
00:09:43,200 --> 00:09:46,790
De la base de datos la aplicación se ha elegido por su variedad

147
00:09:46,800 --> 00:09:49,690
de tecnología demostrando las bondades de los contenedores como

148
00:09:49,700 --> 00:09:52,590
la encapsulación y la portabilidad además nos permite una

149
00:09:52,600 --> 00:09:56,690
distribución de las aplicaciones en múltiples nodos para las pruebas de orquestación.

150
00:09:58,000 --> 00:10:02,190
Para comprobar las funcionalidades y el cumplimiento de los requisitos

151
00:10:02,200 --> 00:10:06,190
de la plataforma es necesario emplear múltiples escenarios que nos permiten

152
00:10:06,200 --> 00:10:11,090
verificar el comportamiento esperado en cada uno de ellos plantean tres escenarios,

153
00:10:11,100 --> 00:10:14,690
el primero de ellos se plantea el despliegue de la aplicación en un modo simple,

154
00:10:14,700 --> 00:10:16,990
sin orquestación, los otros dos escenarios

155
00:10:17,000 --> 00:10:19,890
trabajan sobre un cluster de Docker Swarm de tres nodos,

156
00:10:19,900 --> 00:10:26,090
aquí debemos distinguir entre los nodos y Worker y  manager, los manager son los responsables de

157
00:10:26,100 --> 00:10:30,990
la planificación y el estado del cluster Docker Swarm son los encargados de detener

158
00:10:31,000 --> 00:10:36,490
arrancar los contenedores y servicios en función de las necesidades 12 worker simplemente

159
00:10:36,500 --> 00:10:38,790
ejecuta los contenedores y proporcionan

160
00:10:38,800 --> 00:10:41,390
la capacidad de procesamiento y almacenamiento de un cluster.

161
00:10:42,500 --> 00:10:45,390
Decir también que a diferencia del primer caso donde solo tenía

162
00:10:45,400 --> 00:10:50,190
un nodo al trabajar en cluster se crea una red overlay que permite

163
00:10:50,200 --> 00:10:53,490
la comunicación entre todos los servicios independientemente del modo

164
00:10:53,500 --> 00:10:59,590
en el que se encuentra el segundo de los escenarios se hace un despliegue de 3 nodos,

165
00:10:59,600 --> 00:11:03,690
como decíamos siendo 2 de worker  y uno de ellos manager y

166
00:11:03,700 --> 00:11:08,790
el último de los escenarios plantea el despliegue sobre tres nodos Manager en este caso.

167
00:11:10,100 --> 00:11:14,390
El primero los escenarios contamos con un solo nodo como decíamos y

168
00:11:14,400 --> 00:11:18,590
un despliegue sin orquestador la imágenes se representa la distribución de los

169
00:11:18,600 --> 00:11:22,690
contenedores y puertos tanto internos como externos y el despliegue mediante

170
00:11:22,700 --> 00:11:26,690
docker-compose podemos distinguir tres funcionalidades cada uno de ellos

171
00:11:26,700 --> 00:11:31,190
correspondiente a un docker-compose por un lado gitlab tanto para

172
00:11:31,200 --> 00:11:36,490
el versionado como gitlab-runner para el portainer con el agente y la interfaz

173
00:11:36,500 --> 00:11:41,690
gráfica y los servicios de nuestra aplicación  con todos sus componentes que

174
00:11:41,700 --> 00:11:43,290
habíamos definido en el apartado anterior.

175
00:11:45,500 --> 00:11:49,490
El segundo de los escenarios trabaja en un cluster de tres nodos dos de ellos

176
00:11:49,500 --> 00:11:54,490
worker y un manager  y se ha planteado dos pruebas una de ellas

177
00:11:54,500 --> 00:11:59,090
trabajaremos en modo réplicas decir una réplica de cada servicio que

178
00:11:59,100 --> 00:12:03,590
se repartirá en los nodos que decidamos en este caso los worker para la segunda

179
00:12:03,600 --> 00:12:07,890
prueba trabajaremos en modo global que desplegará una copia de cada servicio

180
00:12:07,900 --> 00:12:10,690
en todos los nuevos incluidos worker todo esto

181
00:12:10,700 --> 00:12:13,490
 es configurable a través del docker-compose.

182
00:12:15,000 --> 00:12:17,690
Para el nodo réplica,

183
00:12:17,700 --> 00:12:19,790
podemos ver cómo se han repartido los servicios

184
00:12:19,800 --> 00:12:23,090
los servicios de administración administra laisser,

185
00:12:23,100 --> 00:12:26,890
se definen para ser desplegados únicamente en el nodo manager 

186
00:12:26,900 --> 00:12:30,590
en este caso herramienta Visualizer cómo vemos nos permite visualizar

187
00:12:30,600 --> 00:12:34,190
el estado de los servicios y nodos tal y como se muestra en la imagen de la derecha,

188
00:12:34,200 --> 00:12:38,890
el resto de los 5 servicios restantes de nuestra aplicación que

189
00:12:38,900 --> 00:12:42,890
anteriormente son configurados para ser desplegados únicamente en nodos

190
00:12:42,900 --> 00:12:47,890
worker y podemos ver cómo se despliegan todos los servicios en los nodos 2 y 3.

191
00:12:49,000 --> 00:12:53,890
El comportamiento de este escenario en caso de caída de uno de los dos Walker

192
00:12:53,900 --> 00:12:58,290
provocaría una pérdida transitoria del funcionamiento global de la aplicación

193
00:12:58,300 --> 00:13:02,790
durante el tiempo en el que los servicios del nodo son repartidos por el 

194
00:13:02,800 --> 00:13:06,890
orquestador en el otro nodo worker una vez todos los servicios son arrancados

195
00:13:06,900 --> 00:13:11,790
en el nuevo nodo no habría perdido alguna de funcionamiento en cambio una caída

196
00:13:11,800 --> 00:13:15,790
del nodo manager sí que haré una perdida de funcionamiento importante al perder

197
00:13:15,800 --> 00:13:19,990
orquestador no existe gestión de los nodos se pierden herramientas como routing mesh

198
00:13:20,000 --> 00:13:24,190
y no se puede asegurar el funcionamiento correcto del cluster es por lo tanto

199
00:13:24,200 --> 00:13:25,890
un Estado no deseado.

200
00:13:27,800 --> 00:13:32,190
En el siguiente caso continuaremos con el Escenario 2 pero en este caso en modo

201
00:13:32,200 --> 00:13:38,290
global excluyendo a los servicios administrativos adminer y Visualizer que no

202
00:13:38,300 --> 00:13:42,690
se duplicarán se tendrá una copia de cada uno de los cinco servicios de nuestra

203
00:13:42,700 --> 00:13:48,890
aplicación de pruebas en funcionamiento normal es idéntico al caso anterior sin embargo,

204
00:13:48,900 --> 00:13:53,890
sí que tenemos diferencias en caso de fallo en uno de los worker en este caso

205
00:13:53,900 --> 00:13:57,690
la caída de un  nodo worker no supone una pérdida de funcionamiento transistoria 

206
00:13:57,700 --> 00:14:03,190
en  ningún momento y la aplicación seguiría funcionando de forma normal pese

207
00:14:03,200 --> 00:14:07,890
a caída de ellos en caso de pérdida del nodo Manager el comportamiento sí que

208
00:14:07,900 --> 00:14:11,790
es análogo al que acabamos de ver en el caso anterior y el resultado sería

209
00:14:11,800 --> 00:14:12,990
un estado indeseable.

210
00:14:14,000 --> 00:14:19,290
Por último el último de los escenarios consiste en 3 nodos manager

211
00:14:19,300 --> 00:14:23,090
Docker trabaja con el algoritmo raft consenseus que requiere que la mayoría

212
00:14:23,100 --> 00:14:28,290
de los nodos manager estén de acuerdo para realizar un tal y como podemos ver

213
00:14:28,300 --> 00:14:32,790
en la tabla para nuestro caso con tres nodos manager podemos perder uno de ellos

214
00:14:32,800 --> 00:14:37,090
y seguir funcionando correctamente vemos también como en caso de trabajar con

215
00:14:37,100 --> 00:14:40,490
un único manager como seriamente su perdida supone una pérdida de

216
00:14:40,500 --> 00:14:41,990
la funcionalidad del cluster.

217
00:14:42,000 --> 00:14:43,290
Entonces,

218
00:14:43,300 --> 00:14:47,890
cómo se comporta este escenario ante la caída de un nodo manager

219
00:14:47,900 --> 00:14:51,790
está caída provocaría una pérdida transitoria de funcionamiento similar a

220
00:14:51,800 --> 00:14:55,590
la vista en los escenarios anteriores que volverá al funcionamiento nominal

221
00:14:55,600 --> 00:14:59,590
cuando los servicios sean repartidos en los nodos restantes esto es así porque

222
00:14:59,600 --> 00:15:01,690
estaremos trabajando con el modo réplica

223
00:15:01,700 --> 00:15:04,390
si fuese necesario evitar está pérdida transitoria,

224
00:15:04,400 --> 00:15:09,090
se podría combinar la existencia de múltiples nodos y el modo el modo global

225
00:15:09,100 --> 00:15:10,890
evitando de este modo tanto las pérdidas

226
00:15:10,900 --> 00:15:14,690
transitorias  con la caída de un nodo en modo replica,

227
00:15:14,700 --> 00:15:18,590
se debe sin embargo estudiar las necesidades

228
00:15:18,600 --> 00:15:23,190
y recursos de nuestro sistema ya que el de duplicado servicios veas el modo

229
00:15:23,200 --> 00:15:27,390
global como el uso de múltiples manager supone aumento sustancial del uso

230
00:15:27,400 --> 00:15:31,690
de recursos y puede suponer un decremento en el rendimiento de nuestra instalación.

231
00:15:32,600 --> 00:15:38,690
Finalizaremos esta tapa mostrando las conclusiones extraídas del proyecto.

232
00:15:38,700 --> 00:15:41,190
Trataremos estos tres aspectos,

233
00:15:41,200 --> 00:15:45,890
el alcance del proyecto las conclusiones están y las líneas futuras.

234
00:15:47,100 --> 00:15:51,490
Comenzaremos viendo el alcance y seguimiento del proyecto por

235
00:15:51,500 --> 00:15:54,990
un lado el tiempo estimado total era de 300 horas con una carga

236
00:15:55,000 --> 00:15:58,790
mayor para las entregas dos y tres como dijimos sin embargo en estas fases,

237
00:15:58,800 --> 00:16:01,990
dónde se ha producido una mayor desviación debido a

238
00:16:02,000 --> 00:16:05,590
la necesidad de superar las necesidades técnicas en total se

239
00:16:05,600 --> 00:16:10,490
ha superado la carga de trabajo en aproximadamente un 33% y aunque era esperable,

240
00:16:10,500 --> 00:16:15,190
y sea aceptable, de este modo se ha conseguido evitar retrasos en la entrega y se

241
00:16:15,200 --> 00:16:20,790
ha conseguido todos los hitos de la planificación por otro lado

242
00:16:20,800 --> 00:16:22,090
el reparto de la carga de trabajo,

243
00:16:22,100 --> 00:16:25,290
se puede ver  como la implementación y validación del proyecto.

244
00:16:25,300 --> 00:16:29,790
Ha consumido casi la mitad de los recursos temporales y cómo se ha invertido

245
00:16:29,800 --> 00:16:33,890
también una importante cantidad de recursos en la elaboración de una

246
00:16:33,900 --> 00:16:38,590
documentación acorde con el proyecto por último indicar que se ha conseguido

247
00:16:38,600 --> 00:16:40,190
cumplir y validar todos los requisitos

248
00:16:40,200 --> 00:16:43,490
y objetivos propuestos en las primeras fases del proyecto.

249
00:16:45,300 --> 00:16:48,990
Las conclusiones por lo tanto que podemos obtener del proyecto son las siguientes,

250
00:16:49,000 --> 00:16:53,390
se ha conseguido alcanzar los objetivos planteados al inicio del proyecto

251
00:16:53,400 --> 00:16:58,090
el resultado de la plataforma es el esperado y los conocimientos adquiridos son una

252
00:16:58,100 --> 00:17:01,790
buena base para seguir avanzando en el apasionante mundo de los contenedores y

253
00:17:01,800 --> 00:17:06,490
la orquestación en cuanto a la tecnología utilizada docker-swarm se presenta como una

254
00:17:06,500 --> 00:17:10,790
tecnología simple que se integra directamente co Dicker  sin necesidad de 

255
00:17:10,800 --> 00:17:11,990
de software suplementario,

256
00:17:12,000 --> 00:17:16,390
lo cual lo hace ideal para entornos y aplicaciones ya en funcionamiento con un mínimo

257
00:17:16,400 --> 00:17:22,490
de riesgo e inversión también se ha podido comprobar las bondades contenedores nos

258
00:17:22,500 --> 00:17:26,990
ofrecen tanto cuando capsulacion o portabilidad en entornos distribuidos como

259
00:17:27,000 --> 00:17:31,590
el empleado por último resaltar la importancia de la documentación en cada fase del

260
00:17:31,600 --> 00:17:34,090
proyecto como parte de la realización de los

261
00:17:34,100 --> 00:17:37,190
conocimientos y como base para posibles mejoras futuras.

262
00:17:38,600 --> 00:17:40,490
durante la fase de análisis,

263
00:17:40,500 --> 00:17:45,290
algunas funcionalidades estudiadas se desestimaron debido a las restricciones

264
00:17:45,300 --> 00:17:50,290
de tiempo como resultado no sea lograron abordar todos los aspectos necesarios para

265
00:17:50,300 --> 00:17:53,890
cumplir con los requisitos más rigurosos de una plataforma de despliegue

266
00:17:53,900 --> 00:17:57,790
de aplicaciones Docker por ello se proporcionan varias líneas de desarrollo que

267
00:17:57,800 --> 00:18:02,090
podían generar mejoras significativas sobre el modelo actual en un futuro por

268
00:18:02,100 --> 00:18:05,990
un lado la implementación del sistema de gestión de usuarios como LDAP todos

269
00:18:06,000 --> 00:18:11,090
 Por otro lado la implementación de un sistema centralizado

270
00:18:11,100 --> 00:18:15,590
de registro del logs para todas las aplicaciones implementadas en la plataforma como

271
00:18:15,600 --> 00:18:19,290
por ejemplo kibana y por último la implementación de un sistema de redundante

272
00:18:19,300 --> 00:18:23,790
de almacenamiento de datos distribuidos accesibles a todos los nodos del cluster

273
00:18:23,800 --> 00:18:30,390
de contenedores como por ejemplo rex-ray y queremos finalizar esta presentación

274
00:18:30,400 --> 00:18:33,590
agradeciendo al equipo docente apoye guía y paciencia,

275
00:18:33,600 --> 00:18:36,290
los momentos más difíciles y como no a mi familia

276
00:18:36,300 --> 00:18:38,290
y amigos que me han apoyado y ayudado en estos meses,

277
00:18:38,300 --> 00:18:42,090
por supuesto al tribunal también por su atención quedo a

278
00:18:42,100 --> 00:18:46,090
la disposición suya para resolver las dudas o curiosidades que pueda tener.

279
00:18:46,700 --> 00:18:48,190
Gracias de nuevo y un saludo.