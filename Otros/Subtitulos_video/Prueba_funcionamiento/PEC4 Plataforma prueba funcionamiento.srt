1
00:00:00,480 --> 00:00:04,799
Hola soy rol Machos voy a hacer una

2
00:00:02,639 --> 00:00:06,299
demostración en mi plataforma como parte

3
00:00:04,799 --> 00:00:07,799
de mi proyecto final de grado de

4
00:00:06,299 --> 00:00:10,500
ingeniería informática la plataforma

5
00:00:07,799 --> 00:00:12,719
debe ser capaz de desplegar aplicaciones

6
00:00:10,500 --> 00:00:15,179
basadas en contenedores sobre un cluster

7
00:00:12,719 --> 00:00:17,880
de nodos docker con un orquestador

8
00:00:15,179 --> 00:00:20,100
docker en este caso contaremos con tres

9
00:00:17,880 --> 00:00:21,720
nodos que se basarán en máquinas

10
00:00:20,100 --> 00:00:24,359
virtuales sobre virtualbox y sistema

11
00:00:21,720 --> 00:00:26,279
operativo ubuntu aparte tenemos una

12
00:00:24,359 --> 00:00:28,800
cuarta máquina virtual que será la

13
00:00:26,279 --> 00:00:30,840
encargada de disponer datos para que

14
00:00:28,800 --> 00:00:33,360
sean accesibles al resto de nodos

15
00:00:30,840 --> 00:00:34,980
mediante tecnología nfs esto no será muy

16
00:00:33,360 --> 00:00:37,079
útil por ejemplo para las bases de datos

17
00:00:34,980 --> 00:00:38,640
de forma que puedan correr en cualquiera

18
00:00:37,079 --> 00:00:40,200
de los nodos y tener los datos

19
00:00:38,640 --> 00:00:41,760
actualizados

20
00:00:40,200 --> 00:00:43,980
lo primero lo que debemos hacer cuando

21
00:00:41,760 --> 00:00:45,899
recibimos una imagen para desplegar es

22
00:00:43,980 --> 00:00:47,940
analizarla en este caso vamos a

23
00:00:45,899 --> 00:00:50,039
analizarla mediante dos herramientas de

24
00:00:47,940 --> 00:00:51,719
seguridad lo que Security bench que la

25
00:00:50,039 --> 00:00:52,980
oficial de docker y tribu una de las más

26
00:00:51,719 --> 00:00:54,960
empleadas

27
00:00:52,980 --> 00:00:56,760
esto que Security bench nos permitirá

28
00:00:54,960 --> 00:00:58,980
realizar un análisis de nuestro sistema

29
00:00:56,760 --> 00:01:02,899
y obtener un informe y detallado sobre

30
00:00:58,980 --> 00:01:02,899
las vulnerabilidades como podemos ver

31
00:01:03,480 --> 00:01:07,799
por otro lado trivi nos permitirá

32
00:01:05,760 --> 00:01:10,080
realizar un análisis de nuestras

33
00:01:07,799 --> 00:01:12,780
imágenes y ver qué vulnerabilidades

34
00:01:10,080 --> 00:01:15,600
tiene y si podemos desplegarlas o no en

35
00:01:12,780 --> 00:01:17,760
función de nuestro criterio

36
00:01:15,600 --> 00:01:20,520
en este caso analizaremos una de las que

37
00:01:17,760 --> 00:01:21,900
vamos a utilizar luego en nuestra

38
00:01:20,520 --> 00:01:23,659
aplicación

39
00:01:21,900 --> 00:01:26,159
si la aplicación que

40
00:01:23,659 --> 00:01:27,659
recibimos del equipo de desarrollo es

41
00:01:26,159 --> 00:01:29,280
correcta vamos a proceder a su

42
00:01:27,659 --> 00:01:32,340
despliegue vamos a utilizar para ello

43
00:01:29,280 --> 00:01:33,600
nuestra herramienta gitlab  va a

44
00:01:32,340 --> 00:01:36,000
correr también sobre contenedores en

45
00:01:33,600 --> 00:01:40,140
este caso tendremos dos contenedores y

46
00:01:36,000 --> 00:01:42,140
uno para el registro de ficheros y

47
00:01:40,140 --> 00:01:46,820
versionado y por otro lado

48
00:01:42,140 --> 00:01:46,820
del despliegue sobre nuestra plataforma

49
00:01:46,979 --> 00:01:51,360
vemos aquí por ejemplo tenemos nuestro

50
00:01:49,140 --> 00:01:52,979
proyecto que tendrá el docker compose

51
00:01:51,360 --> 00:01:55,500
que tendrá los la configuración de

52
00:01:52,979 --> 00:01:58,020
nuestro proyecto de despliegue de

53
00:01:55,500 --> 00:02:00,180
servicios etcétera y

54
00:01:58,020 --> 00:02:03,960
tendremos aquí nuestra pipeline para

55
00:02:00,180 --> 00:02:07,020
configurar y desplegar nuestras imágenes

56
00:02:03,960 --> 00:02:08,940
y veremos como los últimos despliegues

57
00:02:07,020 --> 00:02:11,220
han sido correctos

58
00:02:08,940 --> 00:02:13,860
por otro lado tenemos el Gitlab runner que

59
00:02:11,220 --> 00:02:17,040
como vemos está online está activo y

60
00:02:13,860 --> 00:02:19,040
puedes desplegar cuando la pipeline se

61
00:02:17,040 --> 00:02:22,040
lo pida aquí con lo que comentábamos

62
00:02:19,040 --> 00:02:24,300
tendríamos nuestros últimos despliegues

63
00:02:22,040 --> 00:02:26,700
una vez se ha desplegado correctamente

64
00:02:24,300 --> 00:02:29,640
la aplicación podemos monitorizar el

65
00:02:26,700 --> 00:02:31,560
estado de nuestra plataforma nos hemos

66
00:02:29,640 --> 00:02:34,099
decantado por la herramienta portainer

67
00:02:31,560 --> 00:02:39,300
que es muy sencilla y potente y permite

68
00:02:34,099 --> 00:02:41,220
realizar varias acciones en este caso en

69
00:02:39,300 --> 00:02:43,319
este caso veremos Cómo tenemos en

70
00:02:41,220 --> 00:02:44,940
nuestro dashboard  está corriendo en

71
00:02:43,319 --> 00:02:48,060
este caso el de  porteiner y el de nuestra

72
00:02:44,940 --> 00:02:51,599
aplicación de ejemplo y tres nodos

73
00:02:48,060 --> 00:02:54,300
tenemos un manager y dos podemos ver los

74
00:02:51,599 --> 00:02:56,519
contenedores que están corriendo los

75
00:02:54,300 --> 00:02:59,780
servicios en este caso que están

76
00:02:56,519 --> 00:03:03,660
corriendo sobre el cluster los volúmenes

77
00:02:59,780 --> 00:03:05,160
las redes y por ejemplo nuestros nodos y

78
00:03:03,660 --> 00:03:07,800
el estado

79
00:03:05,160 --> 00:03:09,840
esta herramienta nos permite también

80
00:03:07,800 --> 00:03:11,599
realizar pequeñas operaciones de crear

81
00:03:09,840 --> 00:03:13,620
nuevos nuevos

82
00:03:11,599 --> 00:03:15,300
servicios etcétera Aunque Generalmente

83
00:03:13,620 --> 00:03:16,280
Nosotros hemos trabajado con línea de

84
00:03:15,300 --> 00:03:18,480
comandos

85
00:03:16,280 --> 00:03:20,400
una vez tenemos desplegado nuestra

86
00:03:18,480 --> 00:03:21,599
aplicación vamos a demostrar el

87
00:03:20,400 --> 00:03:23,940
funcionamiento de esta nuestra

88
00:03:21,599 --> 00:03:27,060
aplicación de ejemplo se trata de votar

89
00:03:23,940 --> 00:03:27,920
sobre nuestra mascota favorita en este

90
00:03:27,060 --> 00:03:31,500
caso

91
00:03:27,920 --> 00:03:33,959
y luego Ver el resultado en una segunda

92
00:03:31,500 --> 00:03:36,599
interfaz los resultados almacenarán en

93
00:03:33,959 --> 00:03:38,720
una base de datos postres

94
00:03:36,599 --> 00:03:40,860
y vamos a utilizar para la demostración

95
00:03:38,720 --> 00:03:42,959
otra herramienta de docker en este caso

96
00:03:40,860 --> 00:03:46,140
visualiser que nos permite ver los

97
00:03:42,959 --> 00:03:48,180
servicios que corren en nuestros nodos

98
00:03:46,140 --> 00:03:49,379
en este caso el reparto de servicios Se

99
00:03:48,180 --> 00:03:51,060
ha realizado de forma que los servicios

100
00:03:49,379 --> 00:03:53,040
de administración en este caso adminer

101
00:03:51,060 --> 00:03:55,980
para el acceso a la base de datos 

102
00:03:53,040 --> 00:03:57,900
portainer son en el nodo manager y el resto

103
00:03:55,980 --> 00:04:00,120
de servicios tanto la base de datos las

104
00:03:57,900 --> 00:04:02,459
interfaces redes etcétera van a correr

105
00:04:00,120 --> 00:04:04,080
sobre los nodos worker

106
00:04:02,459 --> 00:04:07,200
vamos a demostrar cómo funciona también

107
00:04:04,080 --> 00:04:09,420
el routing mesh el routing mesh significa

108
00:04:07,200 --> 00:04:10,980
que nosotros cuando accedemos a un

109
00:04:09,420 --> 00:04:12,180
servicio no tenemos por qué saber en qué

110
00:04:10,980 --> 00:04:14,819
nodo está corriendo en este caso por

111
00:04:12,180 --> 00:04:16,320
ejemplo el visual está corriendo en el

112
00:04:14,819 --> 00:04:19,560
nodo uno como decíamos Pero podemos

113
00:04:16,320 --> 00:04:21,120
acceder a él a través de cualquiera de

114
00:04:19,560 --> 00:04:23,880
los nodos en este caso estaríamos

115
00:04:21,120 --> 00:04:25,500
accediendo a través del nodo 2 y será el

116
00:04:23,880 --> 00:04:27,780
orquestador en el que en el que nos

117
00:04:25,500 --> 00:04:30,360
envíe al nodo Uno que es en el que está

118
00:04:27,780 --> 00:04:33,600
corriendo Igualmente para el nodo 3 Aquí

119
00:04:30,360 --> 00:04:36,360
vemos como el funcionamiento del routing

120
00:04:33,600 --> 00:04:37,979
mesh no tenemos que preocuparnos de dónde

121
00:04:36,360 --> 00:04:39,440
está corriendo el servicio nuestra

122
00:04:37,979 --> 00:04:41,820
Aplicación como decíamos

123
00:04:39,440 --> 00:04:44,400
es tiene datos remanentes en este caso

124
00:04:41,820 --> 00:04:47,240
19 votos y este es el resultado y

125
00:04:44,400 --> 00:04:47,240
podríamos votar

126
00:04:47,540 --> 00:04:53,820
sobre qué mascota queremos y perros o

127
00:04:50,400 --> 00:04:56,580
gatos Por ejemplo aquí votaríamos y los

128
00:04:53,820 --> 00:04:59,160
resultados actualizaría vamos a forzar

129
00:04:56,580 --> 00:05:01,919
un fallo sobre nuestros servicios en

130
00:04:59,160 --> 00:05:04,560
este caso para que los servicios Se

131
00:05:01,919 --> 00:05:06,000
repartan sobre los otros nodos no vamos

132
00:05:04,560 --> 00:05:08,940
a hacerlo de forma ordenada Como podía

133
00:05:06,000 --> 00:05:12,060
ser con un docker  leave o un drain

134
00:05:08,940 --> 00:05:14,960
sino que vamos a simular La retirada de

135
00:05:12,060 --> 00:05:18,300
un nodo mediante quitando el cable de

136
00:05:14,960 --> 00:05:20,220
conexión en este caso vamos a quitar el

137
00:05:18,300 --> 00:05:22,639
de uno de los dos nodos worker por ejemplo el

138
00:05:20,220 --> 00:05:22,639
nodo 3

139
00:05:23,400 --> 00:05:28,380
vale vamos al nodo 3

140
00:05:26,280 --> 00:05:30,600
y vamos a desconectar el cable vamos a

141
00:05:28,380 --> 00:05:33,380
ver cómo se reparten

142
00:05:30,600 --> 00:05:37,440
los servicios visuales en este caso

143
00:05:33,380 --> 00:05:39,740
tardará unos segundos en cambiar  el

144
00:05:37,440 --> 00:05:39,740
nuevo estado

145
00:05:42,479 --> 00:05:46,620
vemos como el nodo 3 ya no está

146
00:05:44,400 --> 00:05:48,600
disponible y los servicios que estaban

147
00:05:46,620 --> 00:05:50,820
corriendo en el nodo 3 ahora van a pasar

148
00:05:48,600 --> 00:05:52,380
al nodo 2  solo el nodo 2 Porque

149
00:05:50,820 --> 00:05:54,240
estos servicios les habíamos indicado

150
00:05:52,380 --> 00:05:56,280
que corrieran solo en nodo worker si no

151
00:05:54,240 --> 00:06:01,380
se repartirían tanto en nodo uno como

152
00:05:56,280 --> 00:06:02,580
nodos aquí vemos como la plataforma ha

153
00:06:01,380 --> 00:06:05,699
repartido bueno en este caso el

154
00:06:02,580 --> 00:06:09,440
orquestador los nuevos servicios

155
00:06:05,699 --> 00:06:09,440
si vamos a nuestro

156
00:06:10,139 --> 00:06:15,060
resultado vemos que sigue funcionando

157
00:06:12,240 --> 00:06:17,900
todo correctamente vamos a devolver de

158
00:06:15,060 --> 00:06:17,900
nuevo el nodo 3

159
00:06:18,180 --> 00:06:22,860
al cluster que vuelve a arrancar y vamos

160
00:06:21,000 --> 00:06:25,319
a ver cómo se comporta la plataforma en

161
00:06:22,860 --> 00:06:28,979
este caso rápidamente el nodo 3 es

162
00:06:25,319 --> 00:06:31,020
detectado y únicamente añade el agente de

163
00:06:28,979 --> 00:06:32,840
portainer que había uno por cada

164
00:06:31,020 --> 00:06:35,400
servicio con lo cual no estaba

165
00:06:32,840 --> 00:06:37,199
por qué no se reparten de nuevo los

166
00:06:35,400 --> 00:06:38,880
servicios porque la orquestador nos va a

167
00:06:37,199 --> 00:06:40,740
minimizar el número de cambios y solo

168
00:06:38,880 --> 00:06:42,800
cuando es forzado como cuando retiramos

169
00:06:40,740 --> 00:06:46,259
un nodo nos hará un balanceo de

170
00:06:42,800 --> 00:06:49,639
servicios y con esto terminamos la

171
00:06:46,259 --> 00:06:49,639
demostración de la plataforma

